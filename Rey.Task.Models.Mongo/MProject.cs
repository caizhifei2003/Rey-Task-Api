﻿using MongoDB.Bson;
using Rey.Data;
using System;
using System.Collections.Generic;

namespace Rey.Task.Models {
    [Repository("task.project")]
    public class MProject : MModel {
        public ObjectId CreateBy { get; set; }
        public DateTime CreateAt { get; set; }
        public string Name { get; set; }
        public List<ObjectId> FavoriteUser { get; set; } = new List<ObjectId>();
        public List<MProjectMember> Members { get; set; } = new List<MProjectMember>();
        public List<ObjectId> Tasks { get; set; } = new List<ObjectId>();
    }

    public class MProjectMember {
        public ObjectId User { get; set; }
    }
}
