﻿using Rey.Data;

namespace Rey.Task.Models {
    [Repository("account.user")]
    public class MUser : MModel {
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Name { get; set; }
        public string HashedPassword { get; set; }
        public string Salt { get; set; }
    }
}
