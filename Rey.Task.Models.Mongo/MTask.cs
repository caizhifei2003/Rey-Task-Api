﻿using MongoDB.Bson;
using Rey.Data;
using System;

namespace Rey.Task.Models {
    [Repository("task.task")]
    public class MTask : MModel {
        public ObjectId CreateBy { get; set; }
        public DateTime CreateAt { get; set; }
        public string Name { get; set; }
    }
}
