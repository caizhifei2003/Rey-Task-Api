﻿using MongoDB.Bson;
using Rey.Data;

namespace Rey.Task.Models {
    public abstract class MModel : IModel<ObjectId> {
        public ObjectId Id { get; set; }
    }
}
