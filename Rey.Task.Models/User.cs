﻿namespace Rey.Task.Models {
    public class User : Model {
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
    }

    public class UserLogin : User {
        public string Password { get; set; }
        public bool Remember { get; set; }
    }

    public class UserRegister : User {
        public string Password { get; set; }
    }
}
