﻿using System;

namespace Rey.Task.Models {
    public class Task : Model {
        public User CreateBy { get; set; }
        public DateTime CreateAt { get; set; } = DateTime.Now;
        public string Name { get; set; }
    }

    public class TaskUpdate {
        public string Name { get; set; }
    }
}
