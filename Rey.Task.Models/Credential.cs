﻿namespace Rey.Task.Models {
    public class Credential {
        public string HashedPassword { get; set; }
        public string Salt { get; set; }
    }
}
