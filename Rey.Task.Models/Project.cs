﻿using System;
using System.Collections.Generic;

namespace Rey.Task.Models {
    public class Project : Model {
        public User CreateBy { get; set; }
        public DateTime CreateAt { get; set; } = DateTime.Now;
        public string Name { get; set; }
        public bool Favorite { get; set; }
        public List<ProjectMember> Members { get; set; } = new List<ProjectMember>();
        public List<Task> Tasks { get; set; } = new List<Task>();
    }

    public class ProjectMember {
        public User User { get; set; }
    }

    public class ProjectUpdate {
        public string Name { get; set; }
    }
}
