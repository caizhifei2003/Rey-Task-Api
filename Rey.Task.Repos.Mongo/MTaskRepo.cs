﻿using MongoDB.Bson;
using Rey.Data;
using Rey.Task.Models;
using Rey.Task.Repos.ModelMapping;
using System.Linq;

namespace Rey.Task.Repos {
    public class MTaskRepo : ITaskRepo {
        private IRepository<MTask> Repository { get; }
        private IModelMapper Mapper { get; }

        public MTaskRepo(IRepository<MTask> repository, IModelMapper mapper) {
            this.Repository = repository;
            this.Mapper = mapper;
        }

        public Models.Task Create(string userId, Models.Task model) {
            var mModel = this.Mapper.Map<MTask>(model);
            mModel.CreateBy = this.Mapper.Map<ObjectId>(userId);
            mModel = this.Repository.InsertOne(mModel);
            return this.Mapper.Map<Models.Task>(mModel, userId);
        }

        public Models.Task Update(string userId, string id, TaskUpdate model) {
            var mUserId = this.Mapper.Map<ObjectId>(userId);
            var mId = this.Mapper.Map<ObjectId>(id);

            var filter = Filter<MTask>.And(and => and
                .Eq(x => x.CreateBy, mUserId)
                .Eq(x => x.Id, mId)
            );

            var update = Update<MTask>.Build(builder => {
                if (model.Name != null) builder.Set(x => x.Name, model.Name);
            });

            var mModel = this.Repository.UpdateOne(filter, update);
            return this.Mapper.Map<Models.Task>(mModel, userId);
        }

        public Models.Task Delete(string userId, string id) {
            var mUserId = this.Mapper.Map<ObjectId>(userId);
            var mId = this.Mapper.Map<ObjectId>(id);

            var filter = Filter<MTask>.And(and => and
                .Eq(x => x.CreateBy, mUserId)
                .Eq(x => x.Id, mId)
            );

            var mModel = this.Repository.DeleteOne(filter);
            return this.Mapper.Map<Models.Task>(mModel, userId);
        }

        public Models.Task Get(string userId, string id) {
            var mUserId = this.Mapper.Map<ObjectId>(userId);
            var mId = this.Mapper.Map<ObjectId>(id);

            var filter = Filter<MTask>.And(and => and
                .Eq(x => x.CreateBy, mUserId)
                .Eq(x => x.Id, mId)
            );

            var mModel = this.Repository.FindOne(filter);
            return this.Mapper.Map<Models.Task>(mModel, userId);
        }

        public QueryResult<Models.Task> Query(string userId, IQueryFilter filter, IQuerySort sort, IQueryPage page) {
            var mUserId = this.Mapper.Map<ObjectId>(userId);

            var _filter1 = Filter<MTask>.Eq(x => x.CreateBy, mUserId);
            var _filter2 = filter.ToFilter<MTask>(mapper => mapper
                .Map("name", (item, builder) => builder.In(x => x.Name, item.Values))
            );
            var _filter = Filter<MTask>.And(_filter1, _filter2);

            var _sort = sort.ToSort<MTask>(mapper => mapper
                .Map("id", (item, builder) => builder.Direction(item.Direction, x => x.Id))
                .Map("name", (item, builder) => builder.Direction(item.Direction, x => x.Name))
            );

            var result = this.Repository.Query(_filter, _sort, page);
            return this.Mapper.Map<QueryResult<Models.Task>>(result, userId);
        }
    }
}
