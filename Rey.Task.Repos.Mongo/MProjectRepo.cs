﻿using MongoDB.Bson;
using Rey.Data;
using Rey.Task.Models;
using Rey.Task.Repos.ModelMapping;
using System.Linq;

namespace Rey.Task.Repos {
    public class MProjectRepo : IProjectRepo {
        private IRepository<MProject> Repository { get; }
        private IModelMapper Mapper { get; }

        public MProjectRepo(IRepository<MProject> repository, IModelMapper mapper) {
            this.Repository = repository;
            this.Mapper = mapper;
        }

        public Project Create(string userId, Project model) {
            var mModel = this.Mapper.Map<MProject>(model);
            mModel.CreateBy = this.Mapper.Map<ObjectId>(userId);
            mModel = this.Repository.InsertOne(mModel);
            return this.Mapper.Map<Project>(mModel, userId);
        }

        public Project Update(string userId, string id, ProjectUpdate model) {
            var mUserId = this.Mapper.Map<ObjectId>(userId);
            var mId = this.Mapper.Map<ObjectId>(id);

            var filter = Filter<MProject>.And(and => and
                .Eq(x => x.CreateBy, mUserId)
                .Eq(x => x.Id, mId)
            );

            var update = Update<MProject>.Build(builder => {
                if (model.Name != null) builder.Set(x => x.Name, model.Name);
            });

            var mModel = this.Repository.UpdateOne(filter, update);
            return this.Mapper.Map<Project>(mModel, userId);
        }

        public Project Delete(string userId, string id) {
            var mUserId = this.Mapper.Map<ObjectId>(userId);
            var mId = this.Mapper.Map<ObjectId>(id);

            var filter = Filter<MProject>.And(and => and
                .Eq(x => x.CreateBy, mUserId)
                .Eq(x => x.Id, mId)
            );

            var mModel = this.Repository.DeleteOne(filter);
            return this.Mapper.Map<Project>(mModel, userId);
        }

        public Project Get(string userId, string id) {
            var mUserId = this.Mapper.Map<ObjectId>(userId);
            var mId = this.Mapper.Map<ObjectId>(id);

            var filter = Filter<MProject>.And(and => and
                .Eq(x => x.CreateBy, mUserId)
                .Eq(x => x.Id, mId)
            );

            var mModel = this.Repository.FindOne(filter);
            return this.Mapper.Map<Project>(mModel, userId);
        }

        public QueryResult<Project> Query(string userId, IQueryFilter filter, IQuerySort sort, IQueryPage page) {
            var mUserId = this.Mapper.Map<ObjectId>(userId);

            var _filter1 = Filter<MProject>.Eq(x => x.CreateBy, mUserId);
            var _filter2 = filter.ToFilter<MProject>(mapper => mapper
                .Map("name", (item, builder) => builder.In(x => x.Name, item.Values))
                .Map("favorite", (item, builder) => {
                    var favorite = bool.Parse(item.Values.First());
                    if (favorite) {
                        builder.AnyEq(x => x.FavoriteUser, mUserId);
                    } else {
                        builder.Not(not => {
                            not.AnyEq(x => x.FavoriteUser, mUserId);
                        });
                    }
                })
            );
            var _filter = Filter<MProject>.And(_filter1, _filter2);

            var _sort = sort.ToSort<MProject>(mapper => mapper
                .Map("id", (item, builder) => builder.Direction(item.Direction, x => x.Id))
                .Map("name", (item, builder) => builder.Direction(item.Direction, x => x.Name))
            );

            var result = this.Repository.Query(_filter, _sort, page);
            return this.Mapper.Map<QueryResult<Project>>(result, userId);
        }

        public void AddFavorite(string userId, string id) {
            var mUserId = this.Mapper.Map<ObjectId>(userId);
            var mId = this.Mapper.Map<ObjectId>(id);

            var filter = Filter<MProject>.Eq(x => x.Id, mId);
            var update = Update<MProject>.AddToSet(x => x.FavoriteUser, mUserId);

            this.Repository
                .UpdateOne(filter, update);
        }

        public void RemoveFavorite(string userId, string id) {
            var mUserId = this.Mapper.Map<ObjectId>(userId);
            var mId = this.Mapper.Map<ObjectId>(id);

            var filter = Filter<MProject>.Eq(x => x.Id, mId);
            var update = Update<MProject>.Pull(x => x.FavoriteUser, mUserId);

            this.Repository
                .UpdateOne(filter, update);
        }

        public void AddMember(string userId, string id, string memberId) {
            var mUserId = this.Mapper.Map<ObjectId>(userId);
            var mId = this.Mapper.Map<ObjectId>(id);
            var mMemberId = this.Mapper.Map<ObjectId>(memberId);

            var filter = Filter<MProject>.And(and => and
                .Eq(x => x.CreateBy, mUserId)
                .Eq(x => x.Id, mId)
            );

            var update = Update<MProject>.AddToSet(x => x.Members, new MProjectMember() { User = mMemberId });

            this.Repository
                .UpdateOne(filter, update);
        }

        public void RemoveMember(string userId, string id, string memberId) {
            var mUserId = this.Mapper.Map<ObjectId>(userId);
            var mId = this.Mapper.Map<ObjectId>(id);
            var mMemberId = this.Mapper.Map<ObjectId>(memberId);

            var filter = Filter<MProject>.And(and => and
                .Eq(x => x.CreateBy, mUserId)
                .Eq(x => x.Id, mId)
            );

            var update = Update<MProject>.PullFilter(x => x.Members, f => f.Eq(x => x.User, mMemberId));

            this.Repository
                .UpdateOne(filter, update);
        }

        public void AddTask(string userId, string id, string taskId) {
            var mUserId = this.Mapper.Map<ObjectId>(userId);
            var mId = this.Mapper.Map<ObjectId>(id);
            var mTaskId = this.Mapper.Map<ObjectId>(taskId);

            var filter = Filter<MProject>.And(and => and
                .Eq(x => x.CreateBy, mUserId)
                .Eq(x => x.Id, mId)
            );

            var update = Update<MProject>.AddToSet(x => x.Tasks, mTaskId);

            this.Repository
                .UpdateOne(filter, update);
        }

        public void RemoveTask(string userId, string id, string taskId) {
            var mUserId = this.Mapper.Map<ObjectId>(userId);
            var mId = this.Mapper.Map<ObjectId>(id);
            var mTaskId = this.Mapper.Map<ObjectId>(taskId);

            var filter = Filter<MProject>.And(and => and
                .Eq(x => x.CreateBy, mUserId)
                .Eq(x => x.Id, mId)
            );

            var update = Update<MProject>.Pull(x => x.Tasks, mTaskId);

            this.Repository
                .UpdateOne(filter, update);
        }
    }
}
