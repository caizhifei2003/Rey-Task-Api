﻿using Rey.Data;
using Rey.Task.Repos;
using Rey.Task.Repos.ModelMapping;
using Rey.Task.Repos.ModelMapping.Profiles;
using System;

namespace Microsoft.Extensions.DependencyInjection {
    public static class MongoServiceCollectionExtensions {
        public static IServiceCollection AddMongoRepos(this IServiceCollection services, Action<ReyMongoClientOptions> configure) {
            var options = new ReyMongoClientOptions();
            configure(options);

            services.AddSingleton<IReyMongoClientOptions>(options);
            services.AddSingleton<IClient, ReyMongoClient>();
            services.AddSingleton<IDatabase, ReyMongoDatabase>();
            services.AddSingleton(typeof(IRepository<>), typeof(ReyMongoRepository<>));
            services.AddSingleton(typeof(IModelRepository<,>), typeof(ReyMongoModelRepository<,>));

            services.AddSingleton<IModelMapper, ModelMapper>();
            services.AddSingleton<IModelMapperProfile, IdProfile>();
            services.AddSingleton<IModelMapperProfile, UserProfile>();
            services.AddSingleton<IModelMapperProfile, ProjectProfile>();
            services.AddSingleton<IModelMapperProfile, TaskProfile>();

            services.AddSingleton<IUserRepo, MUserRepo>();
            services.AddSingleton<IProjectRepo, MProjectRepo>();
            services.AddSingleton<ITaskRepo, MTaskRepo>();

            return services;
        }
    }
}

