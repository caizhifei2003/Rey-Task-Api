﻿using System;
using System.Collections.Generic;

namespace Rey.Task.Repos.ModelMapping {
    public interface IModelMapper {
        TResult Map<TResult>(object model, Action<IDictionary<string, object>> data = null);
        TResult Map<TResult>(object model, string userId);
        TResult Map<TModel, TResult>(TModel model, Action<IDictionary<string, object>> data = null);
        TResult Map<TModel, TResult>(TModel model, string userId);
        TResult Map<TModel, TResult>(TModel model, TResult result, Action<IDictionary<string, object>> data = null);
        TResult Map<TModel, TResult>(TModel model, TResult result, string userId);
    }
}
