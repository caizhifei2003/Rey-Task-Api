﻿using AutoMapper;

namespace Rey.Task.Repos.ModelMapping {
    public interface IModelMapperProfile {
        void Configure(IMapperConfigurationExpression mapper);
    }
}
