﻿using AutoMapper;
using Rey.Data;
using System;
using System.Collections.Generic;

namespace Rey.Task.Repos.ModelMapping {
    public class ModelMapper : IModelMapper {


        private IMapper Mapper { get; }
        private IDatabase Database { get; }
        private IEnumerable<IModelMapperProfile> Profiles { get; }

        public ModelMapper(IDatabase database, IEnumerable<IModelMapperProfile> profiles) {
            this.Database = database;
            this.Profiles = profiles;
            this.Mapper = new MapperConfiguration(this.Configure)
                .CreateMapper();
        }

        public TResult Map<TResult>(object model, Action<IDictionary<string, object>> data = null) {
            return this.Mapper.Map<TResult>(model, options => {
                data?.Invoke(options.Items);
            });
        }

        public TResult Map<TResult>(object model, string userId) {
            return this.Map<TResult>(model, data => data[DataKeys.USER_ID] = userId);
        }

        public TResult Map<TModel, TResult>(TModel model, Action<IDictionary<string, object>> data = null) {
            return this.Mapper.Map<TModel, TResult>(model, options => {
                data?.Invoke(options.Items);
            });
        }

        public TResult Map<TModel, TResult>(TModel model, string userId) {
            return this.Map<TModel, TResult>(model, data => data[DataKeys.USER_ID] = userId);
        }

        public TResult Map<TModel, TResult>(TModel model, TResult result, Action<IDictionary<string, object>> data = null) {
            return this.Mapper.Map<TModel, TResult>(model, result, options => {
                data?.Invoke(options.Items);
            });
        }

        public TResult Map<TModel, TResult>(TModel model, TResult result, string userId) {
            return this.Map<TModel, TResult>(model, result, data => data[DataKeys.USER_ID] = userId);
        }

        protected virtual void Configure(IMapperConfigurationExpression mapper) {
            foreach (var profile in this.Profiles) {
                profile.Configure(mapper);
            }
        }
    }
}
