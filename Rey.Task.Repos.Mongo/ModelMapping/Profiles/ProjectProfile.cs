﻿using AutoMapper;
using MongoDB.Bson;
using Rey.Task.Models;
using System;
using System.Linq;

namespace Rey.Task.Repos.ModelMapping.Profiles {
    public class ProjectProfile : IModelMapperProfile {
        public void Configure(IMapperConfigurationExpression mapper) {
            mapper.CreateMap<Project, MProject>();
            mapper.CreateMap<MProject, Project>();
            mapper.CreateMap<MProject, Project>()
                .ForMember(x => x.Favorite, opt => opt.Ignore())
                .AfterMap(this.ToFavorite);
        }

        private void ToFavorite(MProject from, Project to, ResolutionContext context) {
            if (from == null)
                return;

            if (!context.Items.TryGetValue(DataKeys.USER_ID, out var userId)) {
                throw new InvalidOperationException("cannot map favorite without current user");
            }

            var mUserId = context.Mapper.Map<ObjectId>(userId as string);
            to.Favorite = from.FavoriteUser.Any(id => id.Equals(mUserId));
        }
    }
}
