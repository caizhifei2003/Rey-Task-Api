﻿using AutoMapper;
using MongoDB.Bson;

namespace Rey.Task.Repos.ModelMapping.Profiles {
    public class IdProfile : IModelMapperProfile {
        public void Configure(IMapperConfigurationExpression mapper) {
            mapper.CreateMap<string, ObjectId>().ConvertUsing(this.String2ObjectId);
            mapper.CreateMap<ObjectId, string>().ConvertUsing(this.ObjectId2String);
        }

        private ObjectId String2ObjectId(string id) {
            return string.IsNullOrEmpty(id) ? ObjectId.Empty : new ObjectId(id);
        }

        private string ObjectId2String(ObjectId id) {
            return id.ToString();
        }
    }
}
