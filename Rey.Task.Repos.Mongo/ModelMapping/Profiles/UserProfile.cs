﻿using AutoMapper;
using MongoDB.Bson;
using Rey.Data;
using Rey.Task.Models;

namespace Rey.Task.Repos.ModelMapping.Profiles {
    public class UserProfile : IModelMapperProfile {
        private IRepository<MUser> Repository { get; }

        public UserProfile(IRepository<MUser> repository) {
            this.Repository = repository;
        }

        public void Configure(IMapperConfigurationExpression mapper) {
            mapper.CreateMap<User, MUser>();
            mapper.CreateMap<MUser, User>();
            mapper.CreateMap<User, ObjectId>().ConstructUsing(this.User2ObjectId);
            mapper.CreateMap<ObjectId, User>().ConstructUsing(this.ObjectId2User);
        }

        private ObjectId User2ObjectId(User model) {
            var id = model?.Id;
            return string.IsNullOrEmpty(id) ? ObjectId.Empty : new ObjectId(id);
        }

        private User ObjectId2User(ObjectId id, ResolutionContext context) {
            var mModel = this.Repository.FindOne(filter => filter.Eq(x => x.Id, id));
            if (mModel == null)
                return null;

            return context.Mapper.Map<User>(mModel);
        }
    }
}
