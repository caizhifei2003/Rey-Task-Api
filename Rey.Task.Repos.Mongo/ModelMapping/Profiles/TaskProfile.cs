﻿using AutoMapper;
using MongoDB.Bson;
using Rey.Data;
using Rey.Task.Models;

namespace Rey.Task.Repos.ModelMapping.Profiles {
    public class TaskProfile : IModelMapperProfile {
        private IRepository<MTask> Repository { get; }

        public TaskProfile(IRepository<MTask> repository) {
            this.Repository = repository;
        }

        public void Configure(IMapperConfigurationExpression mapper) {
            mapper.CreateMap<Models.Task, ObjectId>().ConstructUsing(this.Task2ObjectId);
            mapper.CreateMap<ObjectId, Models.Task>().ConstructUsing(this.ObjectId2Task);
        }

        private ObjectId Task2ObjectId(Models.Task model) {
            var id = model?.Id;
            return string.IsNullOrEmpty(id) ? ObjectId.Empty : new ObjectId(id);
        }

        private Models.Task ObjectId2Task(ObjectId id, ResolutionContext context) {
            var mModel = this.Repository.FindOne(filter => filter.Eq(x => x.Id, id));
            if (mModel == null)
                return null;

            return context.Mapper.Map<Models.Task>(mModel);
        }
    }
}
