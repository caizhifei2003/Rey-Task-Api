﻿using Rey.Data;
using Rey.Task.Models;
using Rey.Task.Repos.ModelMapping;
using System;

namespace Rey.Task.Repos {
    public class MUserRepo : IUserRepo {
        private IRepository<MUser> Repository { get; }
        private IModelMapper Mapper { get; }

        public MUserRepo(IRepository<MUser> repository, IModelMapper mapper) {
            this.Repository = repository;
            this.Mapper = mapper;
        }

        public bool EmailExist(string email) {
            return this.Repository.Exist(filter => filter.Eq(x => x.Email, email));
        }

        public bool MobileExist(string mobile) {
            return this.Repository.Exist(filter => filter.Eq(x => x.Mobile, mobile));
        }

        public User CredentialInsertOne(User user, Credential credential) {
            var mUser = this.Mapper.Map<MUser>(user);
            mUser.Salt = credential.Salt;
            mUser.HashedPassword = credential.HashedPassword;
            mUser = this.Repository.InsertOne(mUser);
            return this.Mapper.Map<User>(mUser);
        }

        public (User, Credential) CredentialFindOneByEmail(string email) {
            var mUser = this.Repository.FindOne(filter => filter.Eq(x => x.Email, email));
            return (this.Mapper.Map<User>(mUser), this.Mapper.Map<Credential>(mUser));
        }

        public (User, Credential) CredentialFindOneByMobile(string mobile) {
            throw new NotImplementedException();
        }
    }
}
