﻿using Microsoft.AspNetCore.Mvc;
using Rey.Task.Models;
using System.Security.Claims;

namespace Rey.Task.Api {
    public static class ControllerExtensions {
        public static string UserId(this Controller controller) {
            return controller.User.FindFirst(ClaimTypes.NameIdentifier).Value;
        }

        public static User UserWithId(this Controller controller) {
            return new User() { Id = controller.UserId() };
        }
    }
}
