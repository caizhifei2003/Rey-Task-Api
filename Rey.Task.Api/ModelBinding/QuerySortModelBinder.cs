﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Rey.Data;
using Tasks = System.Threading.Tasks;

namespace Rey.Task.Api.ModelBinding {
    public class QuerySortModelBinder : IModelBinder {
        public Tasks.Task BindModelAsync(ModelBindingContext bindingContext) {
            var query = bindingContext.ActionContext.HttpContext.Request.Query;
            var sort = QuerySort.Parse(query);
            bindingContext.Result = ModelBindingResult.Success(sort);
            return Tasks.Task.CompletedTask;
        }
    }
}
