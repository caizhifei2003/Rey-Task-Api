﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Rey.Data;
using Tasks = System.Threading.Tasks;

namespace Rey.Task.Api.ModelBinding {
    public class QueryPageModelBinder : IModelBinder {
        public Tasks.Task BindModelAsync(ModelBindingContext bindingContext) {
            var query = bindingContext.ActionContext.HttpContext.Request.Query;
            var page = QueryPage.Parse(query);
            bindingContext.Result = ModelBindingResult.Success(page);
            return Tasks.Task.CompletedTask;
        }
    }
}
