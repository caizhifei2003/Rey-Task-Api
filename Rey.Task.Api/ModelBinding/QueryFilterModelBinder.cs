﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Rey.Data;
using Tasks = System.Threading.Tasks;

namespace Rey.Task.Api.ModelBinding {
    public class QueryFilterModelBinder : IModelBinder {
        public Tasks.Task BindModelAsync(ModelBindingContext bindingContext) {
            var query = bindingContext.ActionContext.HttpContext.Request.Query;
            var filter = QueryFilter.Parse(query);
            bindingContext.Result = ModelBindingResult.Success(filter);
            return Tasks.Task.CompletedTask;
        }
    }
}
