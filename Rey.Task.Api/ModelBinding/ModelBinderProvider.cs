﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Rey.Task.Api.ModelBinding {
    public class ModelBinderProvider<TModel, TModelBinder> : IModelBinderProvider
        where TModel : class
        where TModelBinder : class, IModelBinder, new() {
        public IModelBinder GetBinder(ModelBinderProviderContext context) {
            if (context.Metadata.ModelType.Equals(typeof(TModel))) {
                return new TModelBinder();
            }
            return null;
        }
    }
}
