﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Rey.Task.Api.ExceptionHandle;

namespace Rey.Task.Api.Filters {
    public class ApiExceptionFilter : IExceptionFilter {
        private IHostingEnvironment Env { get; }

        public ApiExceptionFilter(IHostingEnvironment env) {
            Env = env;
        }

        public void OnException(ExceptionContext context) {
            var exception = context.Exception;
            var resp = new {
                data = new { },
                error = this.Env.IsDevelopment() ? new DebugApiError(exception) : new ApiError(exception)
            };
            context.Result = new JsonResult(resp);
        }
    }
}
