﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Rey.Data;
using Rey.Task.Api.Filters;
using Rey.Task.Api.ModelBinding;
using System.IO;

namespace Rey.Task.Api {
    public class Startup {
        private static readonly string PATH_CONFIG = Path.Combine(Directory.GetCurrentDirectory(), "config");

        private IHostingEnvironment Env { get; }
        private bool IsDevelopment => this.Env.IsDevelopment();

        public Startup(IHostingEnvironment env) {
            this.Env = env;
        }

        public void ConfigureServices(IServiceCollection services) {
            services.AddMvc(options => {
                options.Filters.Add<ApiExceptionFilter>();
                options.Filters.Add<ApiResultFilter>();
                options.ModelBinderProviders.Insert(0, new ModelBinderProvider<IQueryFilter, QueryFilterModelBinder>());
                options.ModelBinderProviders.Insert(1, new ModelBinderProvider<IQuerySort, QuerySortModelBinder>());
                options.ModelBinderProviders.Insert(2, new ModelBinderProvider<IQueryPage, QueryPageModelBinder>());
            });

            services.AddReyServices(rey => {
                rey.Password();
                rey.Cors(options => this.BindFileConfig(options, "cors.json", "cors.prod.json"));
                rey.Jwt(opts => this.BindFileConfig(opts, "jwt.json"));
            });

            services.AddMongoRepos(options => this.BindFileConfig(options, "repo.mongo.json"));
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseConfigurableCors();
            app.UseAuthentication();
            app.UseMvc();
        }

        private void BindFileConfig(object options, string path, string prodPath = null) {
            var configPath = path;
            if (!this.IsDevelopment) {
                configPath = prodPath ?? path;
            }

            new ConfigurationBuilder()
                .SetBasePath(PATH_CONFIG)
                .AddJsonFile(configPath)
                .Build()
                .Bind(options);
        }
    }
}
