﻿using System;
using System.Reflection;

namespace Rey.Task.Api.ExceptionHandle {
    public class ApiException : Exception {
        public ApiExceptionCode Code { get; }
        public ApiException(ApiExceptionCode code, string message)
            : base(message) {
            this.Code = code;
        }

        public static ApiException GetException(ApiExceptionCode code, string message = null) {
            return new ApiException(code, message ?? GetCodeMessage(code));
        }

        public static string GetCodeMessage(ApiExceptionCode code) {
            var type = typeof(ApiExceptionCode);
            return type.GetField(Enum.GetName(type, code))
                .GetCustomAttribute<CodeMessageAttribute>()?
                .Message;
        }
    }
}
