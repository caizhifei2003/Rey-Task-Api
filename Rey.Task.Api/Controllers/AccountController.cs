﻿using Microsoft.AspNetCore.Mvc;
using Rey.Services;
using Rey.Task.Api.ExceptionHandle;
using Rey.Task.Models;
using Rey.Task.Repos;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Rey.Task.Api.Controllers {
    [Route("[controller]")]
    public class AccountController : Controller {
        private IPasswordHasher Hasher { get; }
        private IPasswordVerifier Verifiter { get; }
        private IJwtTokenGenerator TokenGenerator { get; }
        private IUserRepo UserRepo { get; }

        public AccountController(
            IPasswordHasher hasher,
            IPasswordVerifier verifier,
            IJwtTokenGenerator tokenGenerator,
            IUserRepo userRepo) {
            this.Hasher = hasher;
            this.Verifiter = verifier;
            this.TokenGenerator = tokenGenerator;
            this.UserRepo = userRepo;
        }

        [HttpPost("register")]
        public User Register([FromBody]UserRegister user) {
            if (string.IsNullOrEmpty(user.Email))
                throw ApiException.GetException(ApiExceptionCode.EMPTY_EMAIL);

            if (string.IsNullOrEmpty(user.Password))
                throw ApiException.GetException(ApiExceptionCode.EMPTY_PASSWORD);

            if (this.UserRepo.EmailExist(user.Email))
                throw ApiException.GetException(ApiExceptionCode.EXIST_EMAIL);

            var (hashed, salt) = this.Hasher.Hash(user.Password);
            return this.UserRepo.CredentialInsertOne(user, new Credential { HashedPassword = hashed, Salt = salt });
        }

        [HttpPost("login")]
        public object Login([FromBody] UserLogin user) {
            user.Email = user.Email?.ToLower().Trim();

            if (string.IsNullOrEmpty(user.Email))
                throw ApiException.GetException(ApiExceptionCode.EMPTY_EMAIL);

            if (string.IsNullOrEmpty(user.Password))
                throw ApiException.GetException(ApiExceptionCode.EMPTY_PASSWORD);

            var (_user, _cre) = this.UserRepo.CredentialFindOneByEmail(user.Email);
            if (_user == null)
                throw ApiException.GetException(ApiExceptionCode.INVALID_EMAIL);

            if (!this.Verifiter.Verify(user.Password, _cre.HashedPassword, _cre.Salt))
                throw ApiException.GetException(ApiExceptionCode.INVALID_PASSWORD);

            var claims = new List<Claim> {
                new Claim(JwtRegisteredClaimNames.Sub, _user.Id),
                new Claim(JwtRegisteredClaimNames.Email, _user.Email)
            };

            var token = this.TokenGenerator.Generate(claims);

            return new {
                token = token.ToString(),
                timestamp = token.Timestamp
            };
        }
    }
}
