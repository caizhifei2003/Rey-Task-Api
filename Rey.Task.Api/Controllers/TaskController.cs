﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rey.Data;
using Rey.Task.Models;
using Rey.Task.Repos;

namespace Rey.Task.Api.Controllers {
    [Route("[controller]")]
    [Authorize]
    public class TaskController : Controller {
        private ITaskRepo TaskRepo { get; }

        public TaskController(ITaskRepo taskRepo) {
            this.TaskRepo = taskRepo;
        }

        [HttpPost]
        public Models.Task Create([FromBody]Models.Task model) {
            return this.TaskRepo.Create(this.UserId(), model);
        }

        [HttpPut("{id}")]
        public Models.Task Update(string id, [FromBody]TaskUpdate model) {
            return this.TaskRepo.Update(this.UserId(), id, model);
        }

        [HttpDelete("{id}")]
        public Models.Task Delete(string id) {
            return this.TaskRepo.Delete(this.UserId(), id);
        }

        [HttpGet("{id}")]
        public Models.Task Get(string id) {
            return this.TaskRepo.Get(this.UserId(), id);
        }

        [HttpGet]
        public object Query(IQueryFilter filter, IQuerySort sort, IQueryPage page) {
            return this.TaskRepo.Query(this.UserId(), filter, sort, page);
        }
    }
}
