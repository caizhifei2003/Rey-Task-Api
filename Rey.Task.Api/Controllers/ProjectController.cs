﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rey.Data;
using Rey.Task.Models;
using Rey.Task.Repos;

namespace Rey.Task.Api.Controllers {
    [Route("[controller]")]
    [Authorize]
    public class ProjectController : Controller {
        private IProjectRepo ProjectRepo { get; }

        public ProjectController(IProjectRepo projectRepo) {
            this.ProjectRepo = projectRepo;
        }

        [HttpPost]
        public Project Create([FromBody]Project model) {
            return this.ProjectRepo.Create(this.UserId(), model);
        }

        [HttpPut("{id}")]
        public Project Update(string id, [FromBody]ProjectUpdate model) {
            return this.ProjectRepo.Update(this.UserId(), id, model);
        }

        [HttpDelete("{id}")]
        public Project Delete(string id) {
            return this.ProjectRepo.Delete(this.UserId(), id);
        }

        [HttpGet("{id}")]
        public Project Get(string id) {
            return this.ProjectRepo.Get(this.UserId(), id);
        }

        [HttpGet]
        public object Query(IQueryFilter filter, IQuerySort sort, IQueryPage page) {
            return this.ProjectRepo.Query(this.UserId(), filter, sort, page);
        }

        [HttpPost("{id}/favorite/add")]
        public void AddFavorite(string id) {
            this.ProjectRepo.AddFavorite(this.UserId(), id);
        }

        [HttpPost("{id}/favorite/remove")]
        public void RemoveFavorite(string id) {
            this.ProjectRepo.RemoveFavorite(this.UserId(), id);
        }

        [HttpPost("{id}/member/add")]
        public void AddMember(string id, [FromBody]dynamic model) {
            this.ProjectRepo.AddMember(this.UserId(), id, (string)model.memberId);
        }

        [HttpPost("{id}/member/remove")]
        public void RemoveMember(string id, [FromBody]dynamic model) {
            this.ProjectRepo.RemoveMember(this.UserId(), id, (string)model.memberId);
        }

        [HttpPost("{id}/task/add")]
        public void AddTask(string id, [FromBody]dynamic model) {
            this.ProjectRepo.AddTask(this.UserId(), id, (string)model.taskId);
        }

        [HttpPost("{id}/task/remove")]
        public void RemoveTask(string id, [FromBody]dynamic model) {
            this.ProjectRepo.RemoveTask(this.UserId(), id, (string)model.taskId);
        }
    }
}
