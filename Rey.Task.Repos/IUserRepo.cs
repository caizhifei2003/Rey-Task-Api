﻿using Rey.Task.Models;

namespace Rey.Task.Repos {
    public interface IUserRepo {
        bool EmailExist(string email);
        bool MobileExist(string mobile);

        User CredentialInsertOne(User user, Credential credential);
        (User, Credential) CredentialFindOneByEmail(string email);
        (User, Credential) CredentialFindOneByMobile(string mobile);
    }
}
