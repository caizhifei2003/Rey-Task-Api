﻿using Rey.Data;
using Rey.Task.Models;

namespace Rey.Task.Repos {
    public interface IProjectRepo {
        Project Create(string userId, Project model);
        Project Update(string userId, string id, ProjectUpdate model);
        Project Delete(string userId, string id);
        Project Get(string userId, string id);
        QueryResult<Project> Query(string userId, IQueryFilter filter, IQuerySort sort, IQueryPage page);

        void AddFavorite(string userId, string id);
        void RemoveFavorite(string userId, string id);

        void AddMember(string userId, string id, string memberId);
        void RemoveMember(string userId, string id, string memberId);

        void AddTask(string userId, string id, string taskId);
        void RemoveTask(string userId, string id, string taskId);
    }
}
