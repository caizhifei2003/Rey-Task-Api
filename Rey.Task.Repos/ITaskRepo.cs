﻿using Rey.Data;
using Rey.Task.Models;

namespace Rey.Task.Repos {
    public interface ITaskRepo {
        Models.Task Create(string userId, Models.Task model);
        Models.Task Update(string userId, string id, TaskUpdate model);
        Models.Task Delete(string userId, string id);
        Models.Task Get(string userId, string id);
        QueryResult<Models.Task> Query(string userId, IQueryFilter filter, IQuerySort sort, IQueryPage page);
    }
}
