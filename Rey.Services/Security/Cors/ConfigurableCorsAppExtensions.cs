﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Microsoft.AspNetCore.Builder {
    public static class ConfigurableCorsAppExtensions {
        public static IApplicationBuilder UseConfigurableCors(this IApplicationBuilder app) {
            var env = app.ApplicationServices.GetService<IHostingEnvironment>();
            if (env.IsDevelopment()) {
                app.UseCors("Development");
            } else {
                app.UseCors("Production");
            }
            return app;
        }
    }
}
