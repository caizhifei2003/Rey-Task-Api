﻿using Microsoft.IdentityModel.Tokens;

namespace Rey.Services {
    public interface IJwtOptions {
        string Issuer { get; }
        string Audience { get; }
        double ExpiresIn { get; }
        string Key { get; }
        SecurityKey GetSecureKey();
    }
}
