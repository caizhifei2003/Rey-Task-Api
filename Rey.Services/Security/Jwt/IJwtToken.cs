﻿using System;

namespace Rey.Services {
    public interface IJwtToken {
        double ExpiresIn { get; }
        DateTime Date { get; }
        long Timestamp { get; }
        string ToString();
    }
}
