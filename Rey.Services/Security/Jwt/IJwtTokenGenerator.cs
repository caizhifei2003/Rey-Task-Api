﻿using System.Collections.Generic;
using System.Security.Claims;

namespace Rey.Services {
    public interface IJwtTokenGenerator {
        IJwtToken Generate(IEnumerable<Claim> claims, double? expiresIn = null);
    }
}
