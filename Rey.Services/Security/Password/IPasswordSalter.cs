﻿namespace Rey.Services {
    public interface IPasswordSalter {
        byte[] Generate();
        string GenerateString();
    }
}
