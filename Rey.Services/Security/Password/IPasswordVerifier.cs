﻿namespace Rey.Services {
    public interface IPasswordVerifier {
        bool Verify(string password, string hashed, string salt);
    }
}
