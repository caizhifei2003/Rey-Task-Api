﻿namespace Rey.Services {
    public class PasswordVerifier : IPasswordVerifier {
        private IPasswordHasher Hasher { get; }

        public PasswordVerifier(IPasswordHasher hasher) {
            this.Hasher = hasher;
        }

        public bool Verify(string password, string hashed, string salt) {
            var (_hashed, _) = this.Hasher.Hash(password, salt);
            return hashed.Equals(_hashed);
        }
    }
}
