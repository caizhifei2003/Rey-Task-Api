﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;

namespace Rey.Services {
    public class PasswordHasher : IPasswordHasher {
        private IPasswordSalter Salter { get; }

        public PasswordHasher(IPasswordSalter salter = null) {
            this.Salter = salter;
        }

        public (string hashed, string salt) Hash(string password, string salt = null) {
            if (password == null)
                throw new ArgumentNullException(nameof(password));

            salt = salt ?? this.Salter.GenerateString();

            byte[] hashed = KeyDerivation.Pbkdf2(
                password: password,
                salt: Convert.FromBase64String(salt),
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8);

            return (hashed: Convert.ToBase64String(hashed), salt: salt);
        }
    }
}
