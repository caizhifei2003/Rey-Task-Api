﻿using System;
using System.Security.Cryptography;

namespace Rey.Services {
    public class PasswordSalter : IPasswordSalter {
        public byte[] Generate() {
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create()) {
                rng.GetBytes(salt);
            }
            return salt;
        }

        public string GenerateString() {
            return Convert.ToBase64String(this.Generate());
        }
    }
}
