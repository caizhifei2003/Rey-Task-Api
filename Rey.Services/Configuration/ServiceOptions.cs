﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;

namespace Rey.Services.Configuration {
    public class ServiceOptions : IServiceOptions {
        private IServiceCollection Services { get; }

        public ServiceOptions(IServiceCollection services) {
            this.Services = services;
        }

        public IServiceOptions Password() {
            this.Services.TryAddSingleton<IPasswordSalter, PasswordSalter>();
            this.Services.TryAddSingleton<IPasswordHasher, PasswordHasher>();
            this.Services.TryAddSingleton<IPasswordVerifier, PasswordVerifier>();
            return this;
        }

        public IServiceOptions Cors(Action<ConfigurableCorsOptions> configure) {
            var options = new ConfigurableCorsOptions();
            configure(options);

            this.Services.AddSingleton(options);
            this.Services.AddCors(op => {
                op.AddPolicy(options.Name, options.ToPolicy());
            });
            return this;
        }

        public IServiceOptions Jwt(Action<JwtOptions> configure) {
            var options = new JwtOptions();
            configure(options);

            this.Services.AddSingleton<IJwtOptions>(options);
            this.Services.AddSingleton<IJwtTokenGenerator, JwtTokenGenerator>();

            this.Services
                .AddAuthentication(opts => {
                    opts.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                }).AddJwtBearer(opts => {
                    opts.TokenValidationParameters = options.GetValidationParameters();
                });
            return this;
        }
    }
}
