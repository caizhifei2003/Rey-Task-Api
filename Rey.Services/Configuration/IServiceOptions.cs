﻿using System;

namespace Rey.Services.Configuration {
    public interface IServiceOptions {
        IServiceOptions Password();
        IServiceOptions Cors(Action<ConfigurableCorsOptions> configure);
        IServiceOptions Jwt(Action<JwtOptions> configure);
    }
}
