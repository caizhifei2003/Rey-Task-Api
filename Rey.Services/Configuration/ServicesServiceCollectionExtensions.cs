﻿using Rey.Services.Configuration;
using System;

namespace Microsoft.Extensions.DependencyInjection {
    public static class ServicesServiceCollectionExtensions {
        public static IServiceCollection AddReyServices(this IServiceCollection services, Action<IServiceOptions> configure) {
            var options = new ServiceOptions(services);
            configure(options);
            return services;
        }
    }
}
